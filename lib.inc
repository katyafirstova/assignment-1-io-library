section .text
 
; Принимает код возврата и завершает текущий процесс

exit: 
    mov  rax, 60 ; 60 - exit syscall
    syscall
    
  

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    	xor rax, rax
	.loop:
		cmp byte [rdi+rax], 0
		je .end ; finish if we get null terminator
		inc rax
		jmp .loop
	.end:
		ret



; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length    
    mov rdx, rax          
    mov rax, 1            ;write syscall
    mov rsi, rdi          ;read string
    mov rdi, 1            ;write in stdout
    syscall
    pop rdi
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi
    mov rdi, rsp
    call print_string
    pop rdi
    ret


; Переводит строку (выводит символ с кодом 0xA)
print_newline:
	mov rdi, 0xA
	jmp print_char
	

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
        xor rax, rax
        mov rax, rdi
        mov r8, 10
	    mov r9, rsp
	    push 0
	.loop:
		xor rdx, rdx
		div r8
		add rdx, '0'
		dec rsp
		mov byte [rsp], dl
		cmp rax, 0
		je .end
		jmp .loop
		
    .end:
		mov rdi, rsp
		push r9
		call print_string
		pop r9
		mov rsp, r9
		ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
     
    cmp rdi, 0
    jge .positive ;
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    
    .positive:
    	     jmp print_uint
    	     

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
	xor rcx, rcx
	.loop:
		mov cl, byte[rdi+rax]
		cmp byte[rsi+rax], cl
		jne .not_equals
		cmp byte[rdi + rax], 0
		je .equals
		inc rax
		jmp .loop
	.not_equals:
		xor rax, rax
		ret 
	.equals:
		mov rax, 1
		ret 

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
        xor rax, rax
        xor rdi, rdi
	    push 0
	    mov rsi, rsp ; top element address
	    mov rdx, 1 ; char length
	    syscall
	    pop rax
	    ret      

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
 xor rcx, rcx           
.loop:
    push rdi               
 	push rsi               
	push rcx               
 	call read_char         
	pop rcx                 
    pop rsi
   	pop rdi
   	cmp rax, 0x20           
    je .check_space            
    cmp rax, 0x9           
    je .check_space
    cmp rax, 0xA           
    je .check_space            
    cmp rax, 0x0           
    je .end                  
    mov [rdi+rcx], rax     
    inc rcx              
    cmp rcx, rsi           
    jl .loop              
    xor rax, rax          
    xor rdx, rdx           
    ret                 
.check_space:
    test rcx, rcx          
    jz .loop 
    
.end:
    mov byte[rdi+rcx], 0    
    mov rax, rdi           
    mov rdx, rcx            
    ret

	    

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
        push rbx
        push r9
        mov rbx, 10
        xor rcx, rcx
        xor rax, rax
	.loop:
        mov r9, [rdi+rcx]     
    	and r9, 0xff           
        cmp r9b, 0             
        je .end               
        cmp r9b, 0x30          
    	jb .end                
    	cmp r9b, 0x39          
    	ja .end                
    	mul rbx                 
    	sub r9b, 0x30          
    	add rax, r9         
        inc rcx                 
    	jmp .loop
	.end:
		mov rdx, rcx
		pop r9
		pop rbx
		ret



          
          
; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push rbx                
    mov rbx, [rdi]          
    and rbx, 0xff          
    cmp bl, '-'           
    je .negative               
    call parse_uint        
    jmp .end                 
.negative:  
    inc rdi               
    call parse_uint         
    test rdx, rdx           
    jz .end               
    neg rax                 
    inc rdx                 
.end:
    pop rbx               
    ret 


; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rcx, rcx
    push rdi
    call string_length
    pop rdi
    cmp rax, rdx
    jg .end
    
    .loop:	    
	    mov rdx, [rdi+rcx]   
	    mov [rsi+rcx], rdx   
	    inc rcx               
	    cmp byte[rdi+rcx], 0   
	    jne .loop              
	    ret
    .end:
	    xor rax, rax          
	    ret
		  
